# Accident Detection System

### Hardware

<center><img src="circuit.png" alt="operation demonstration" width="285"></center>
<br>

The hardware consists of a 3-axis Accelerometer to detect if the vehicle is in an unusual orientation, an LCD display for user feedback, a generic GPS chip to grab location coordinates, a GSM board fixed with a SIM Card to make send an SMS containing location data and calls any assigned emergency numbers. The whole system is powered by an Arduino board.

### Software
0. Make sure to set your desired orientation data and your phone number in the code before uploading.
1. Make the circuit and connect the Arduino board to a computer.
2. Upload ```AccidentDetection.ino``` to the Arduino. You can also burn the respective binaries to individual ATMega328 chips.
3. Once complete, move the accelerometer around. If an unusual orientation is detected, the board texts your current location to the phone number assigned.

[Open-source notices](NOTICE)

<b>License</b>:<br>

<a href="http://www.gnu.org/licenses/gpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/0e71b2b50532b8f93538000b46c70a78007d0117/68747470733a2f2f7777772e676e752e6f72672f67726170686963732f67706c76332d3132377835312e706e67" alt="GNU GPLv3 Image" data-canonical-src="https://www.gnu.org/graphics/gplv3-127x51.png" width="80"></a><br>[Copyright © Owais Shaikh 2019](LICENSE)
<br><br><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" width="70" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Accident Detection System</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Owais Shaikh</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/users/ThomasCat" rel="dct:source">https://gitlab.com/users/ThomasCat</a>.

