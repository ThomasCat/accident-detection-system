/*
    Accident Detection System
    Copyright (C) 2019  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V **/

#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
#include <TinyGPS++.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
SoftwareSerial mySerial(6, 7);
static const int RXPin = 9, TXPin = 8;
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;
const int groundpin = 18;             // analog input pin 4 -- ground
const int powerpin = 19;              // analog input pin 5 -- voltage
const int xpin = A3;                  // x-axis of the accelerometer
const int ypin = A2;                  // y-axis
const int zpin = A1;                  // z-axis (only on 3-axis models)


void setup(){
  // set up the LCD's number of columns and rows:
  
  Serial.begin(9600);
    // print the sensor values:
  Serial.print(analogRead(xpin));
  // print a tab between values:
  Serial.print("\t");
  Serial.print(analogRead(ypin));
  // print a tab between values:
  Serial.print("\t");
  Serial.print(analogRead(zpin));
  Serial.println();
  // delay before next reading:
  delay(100);
  
  //Begin serial communication with Arduino and SIM900
  mySerial.begin(9600);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  // Print a message to the LCD.
  lcd.print("***STARTING***");
  delay(2000);
  lcd.clear();
  // Print a message to the LCD.
  mySerial.println("AT"); //Handshaking with SIM900
  updateSerial();
  /*mySerial.println("AT+CMGF=1"); // Configuring TEXT mode
  updateSerial();
  mySerial.println("AT+CMGS=\"+918369402294\"");//change ZZ with country code and xxxxxxxxxxx with phone number to sms
  updateSerial();
  mySerial.print("Oops."); 
  updateSerial();
  mySerial.write(26);
  lcd.print("Sending SOS..."); */
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print("Done");
  delay(7000);
  lcd.setCursor(0, 1);
  lcd.print("Done");
  lcd.clear();
  mySerial.println("ATDxxxxxxxxxx;"); //Replace this with your phone number
  updateSerial();
  lcd.print("Dialing...");
  delay(5000);
  lcd.setCursor(0, 1);
  lcd.print("Done");
  delay(1000);
}

void loop(){
  SoftwareSerial ss(RXPin, TXPin);
  updateSerial();
  ss.begin(GPSBaud);
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

  if(millis()>5000 && gps.charsProcessed()<10){
    Serial.println(F("No GPS detected: check wiring."));
    updateSerial();
    while(true);
  }
  updateSerial();
}

void updateSerial(){
  delay(500);
  while (Serial.available()){
    mySerial.write(Serial.read());//Forward what Serial received to Software Serial Port
  }
  while(mySerial.available()){
    Serial.write(mySerial.read());//Forward what Software Serial received to Serial Port
  }
}

void displayInfo(){
  Serial.print(F("Location: ")); 
  if(gps.location.isValid()){
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }else{
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if(gps.date.isValid()){
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }else{
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if(gps.time.isValid()){
    if(gps.time.hour() < 10) Serial.print(F("0"));
    	Serial.print(gps.time.hour());
    	Serial.print(F(":"));
    if(gps.time.minute() < 10) Serial.print(F("0"));
    	Serial.print(gps.time.minute());
    	Serial.print(F(":"));
    if(gps.time.second() < 10) Serial.print(F("0"));
    	Serial.print(gps.time.second());
    	Serial.print(F("."));
    if(gps.time.centisecond() < 10) Serial.print(F("0"));
    	Serial.print(gps.time.centisecond());
  }else{
    Serial.print(F("INVALID"));
  }
  Serial.println();
}
